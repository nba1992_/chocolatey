# Chocolatey #

This Chocolatey repository is like DIY to first use this software.

### What is Chocolatey? ###

* [Chocolatey](https://docs.chocolatey.org/en-us/)

### NOTES ###

* Unfortunately, even with non-default folders(D:\\...), you have to execute any of this script with elevated permissions.

### How do I get set up? ###

* Configure $InstallDir at ChocolateySetup.ps1 to the desired install dir. 
* Execute ChocolateySetup.ps1 in order to install Chocolatey software.

### How do I get install packages? ###

* Configure resources/packages.config with the desired software.
* Execute ChocolateyInstallPackages.bat in order to install desired software.

### How do I update packages? ###

* Execute ChocolateyUpdateAllPackages.bat in order to update all chocolatey packages.

### How do I upgrade Chocolatey? ###

* Execute ChocolateyUpgrade.bat.

### Utilities script ###

* ChocolateyInfo.bat.
* ChocolateySources.bat.

### License ###

FREE!!!

### Review/Changes/Maintenance ###

* Feel free to open PR