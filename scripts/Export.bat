echo off
echo # # # # # # # # # # # # #
echo ----- Chocolatey Export -----
echo # # # # # # # # # # # # #

set ARGUMENTS=0

set argC=0
for %%x in (%*) do	(
	echo %%x
	Set /A argC+=1
)
echo arguments:%argC%

if  %argC% NEQ %ARGUMENTS%  (
    echo Parameters missing: #%argC% should be %ARGUMENTS%
    exit 1
)
set LOG_FILENAME=%~n0.log
set GLOBAL_ARGS=--yes --acceptlicense --log-file=%LOG_FILENAME%
set CMD_ARGS=--include-version-numbers --output-file-path="export_packages.config"
echo choco export %GLOBAL_ARGS% %CMD_ARGS%
choco export %GLOBAL_ARGS% %CMD_ARGS%