echo off
echo # # # # # # # # # # # # #
echo ----- Chocolatey Upgrade All -----
echo # # # # # # # # # # # # #

set ARGUMENTS=0

set argC=0
for %%x in (%*) do	(
	echo %%x
	Set /A argC+=1
)
echo arguments:%argC%

if  %argC% NEQ %ARGUMENTS%  (
    echo Parameters missing: #%argC% should be %ARGUMENTS%
    exit 1
)

set LOG_FILENAME=%~n0.log
set GLOBAL_ARGS=--yes --acceptlicense --log-file=%LOG_FILENAME%
echo choco outdated %GLOBAL_ARGS%
choco outdated %GLOBAL_ARGS%
timeout 5
echo choco upgrade all %GLOBAL_ARGS%
choco upgrade all %GLOBAL_ARGS%
timeout 5